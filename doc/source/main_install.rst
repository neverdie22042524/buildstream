

.. _install:

Install
=======

.. include:: release-badge.rst

.. include:: snapshot-badge.rst

This section provides instructions for installing BuildStream and its
companion artifact server on various platforms, along with any installation
related materials.

.. note::

   BuildStream is currently only supported natively on Linux. Users of Unix-like
   systems where Docker is available can still use BuildStream by following the
   :ref:`Docker install guide <docker>`

.. toctree::
   :maxdepth: 1

   install_source
   install_linux_distro
   install_docker
   install_artifacts
   install_versions
